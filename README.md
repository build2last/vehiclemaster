# 融合轨迹数据和开放数据的新能源车画像系统

## 开发、测试的环境和工具
### 语言
* Python 3
* (web) HTML+JS

### 操作系统
* Mac OS 10.10.5
* Windows 10

## 数据库
* GreenPlum 5.0.0+PostGIS-2.1.5
* PostgreSQL+PostGIS-2.1.5

### 测试环境
* Mac OS 10
* Python 3 + django 
* GreenPlum 5.0.0+PostGIS-2.1.5
* PostgreSQL+PostGIS-2.1.5

## 模块与接口
### Conf.py
存储系统的设置和参数等信息，避免hard code是初衷。

### Model: 数据抽象层
希望将基础数据，如轨迹、停留点、兴趣点(POI)等信息进行程序抽象，方便后续分析与操作并更灵活地应对外部数据变化。

### DataProcess: 数据处理模块
对原始数据进行一般正确性检查，从原始数据提取出停留点、POI等基本信息，以及提供给LDA分析的Feature matrix

### DataAnalysis：数据分析模块
在DataProcess和Model的基础上，对数据进行深度分析

### MiddleData文件夹: 中间数据存储模块
存储DataProcess的输出数据

### OUTPUT 文件夹
存储DataAnalysis的分析结果。

### Web: 对数据结果进行展示，尚在建设之中

### requirements.txt[熟悉的人一定很熟悉]
python3 依赖的第三方库。

### test.py
测试模块，python3测试通过

### main.py
系统主函数，尚未完工

## 注意！
* 提取兴趣点算法中涉及到比较多的可调节的参数；
* LDA分析结果没有存储；
* Web相对而言比较独立，其中 settings.py 中涉及到用户密码等敏感信息；
* conf.py 涉及到敏感信息；
* 尚未自动创建以上涉及到的目录文件夹；
* 目前对于环境搭建的过程没有相关文档指导，请自行解决。

## 声明
截止2018-01-12，项目还在开发阶段，未有公开之打算。

Founder：liu kun(lancelotdev@163.com)

[Chinese Version]