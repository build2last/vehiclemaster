# coding:utf-8
from __future__ import division
from __future__ import unicode_literals

"""
利用百度api将地址文本转换为gps坐标

1. 从数据库查询地址文本；
2. 访问百度api转换文本为坐标形式；
3. 将gps以geometry形式存入数据库对应元组中。

2018-01-25
liu kun
(^_^]
"""
__python__ = [2, 3]

import time
from six.moves import urllib
import json
import psycopg2
import conf


KEY = "br6R3ZZwPKACZoToIZUje9oVWB3rL9ot"
baidu_api = "http://api.map.baidu.com/geocoder/v2/?address={address}&output=json&ak={AK}"
TABLE_NAME = "jobad"
QUERY_SQL = "SELECT id, work_place FROM {TABLE_NAME} WHERE work_place_gps is NULL LIMIT {SIZE};"
UPDATE_SQL = "UPDATE {TABLE_NAME} SET work_place_gps=ST_GeomFromText('POINT({lng} {lat})', 4326) WHERE id={row_id};"

DBKW = dict(
        host=conf.DB_HOST,
        port=conf.DB_PORT,
        user=conf.DB_USER,
        database=conf.DB_NAME,
        password=conf.DB_PASSWD
    )

COUNTER = 0

def batch_query(conn, size=0):
    if size > 0: 
        cursor = conn.cursor()
        cursor.execute(QUERY_SQL.format(TABLE_NAME=TABLE_NAME, SIZE=size))
        rows = cursor.fetchall()
        return rows


def single_update(row_id, lat, lng, cursor):
    return cursor.execute(UPDATE_SQL.format(TABLE_NAME=TABLE_NAME, lng=lng, lat=lat, row_id=row_id))


def parse(address):
    """
    外部网络访问模块
    """
    try:
        url = baidu_api.format(address=address, AK=KEY)
        rsp = urllib.request.urlopen(url, timeout=3)
        content = rsp.read()
        gps_json = json.loads(content)
        latitude = gps_json["result"]["location"]["lat"]
        longitude = gps_json["result"]["location"]["lng"]
        level = gps_json["result"]["level"]
        precise = gps_json["result"]["precise"]
        return latitude,longitude
    except KeyError as ke:
        print(gps_json)
        return None


def worker(item_queue, lock):
    conn = psycopg2.connect(
        'dbname={database} user={user} password={password} host={host} port={port}'.format(**DBKW))
    cursor = conn.cursor()
    while not item_queue.empty():
        item = item_queue.get()
        iid, address = item
        lat_lng = parse(urllib.parse.quote(address))
        if lat_lng:
            lat, lng = lat_lng
        else:
            item_queue.task_done()
            continue
        try:
            single_update(row_id=iid, lat=lat, lng=lng, cursor=cursor)
            conn.commit()
            lock.acquire()
            global COUNTER
            COUNTER += 1
            lock.release()
        except Exception as ex:
            conn.rollback()
            print(ex)
        finally:
            item_queue.task_done()
    conn.close()


def parse_rows_MT(rows):
    """ Parse address with multithreads
    """
    num_worker_threads = 10
    import threading
    from Queue import Queue
    COUNTER_LOCK = threading.Lock()
    q = Queue()
    for item in rows:
        q.put(item)
    for i in range(num_worker_threads):
        t = threading.Thread(target=worker, name="worker_"+str(i), args=(q, COUNTER_LOCK))
        t.daemon = True
        t.start()
    q.join()


def main():
    conn = psycopg2.connect(
        'dbname={database} user={user} password={password} host={host} port={port}'.format(**DBKW))
    cursor = conn.cursor()
    rows = batch_query(conn, 2000)
    conn.close()
    parse_rows_MT(rows)
    print("Update success! %d/%d items"%(COUNTER, len(rows)))


if __name__ == '__main__':
    tick = time.time()
    main()
    tock = time.time()
    print("All time cost %d seconds"%(tock-tick))
