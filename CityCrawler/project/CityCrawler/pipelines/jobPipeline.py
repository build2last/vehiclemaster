# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import logging
from twisted.enterprise import adbapi


pipeline_log = open("jobPipeline.log", "w")

def pipe_log(info):
    pipeline_log.write(str(info) + "\n")

CREATE_JOB_TABLE_SQL = (
"""CREATE TABLE IF NOT EXISTS jobad (
      id BIGSERIAL,
      url varchar(100) UNIQUE NOT NULL,
      source varchar(20),
      job_title varchar(100) DEFAULT NULL,
      job_category varchar(50) DEFAULT NULL,
      position_count SMALLINT DEFAULT 0,
      work_place varchar(200) NOT NULL,
      salary_range  varchar(40) NOT NULL,
      salary_ave REAL DEFAULT 0,
      area varchar(50),
      edu varchar(10),
      experience varchar(50),
      company_name varchar(50),
      company_loc varchar(100),
      company_property varchar(50),
      company_online_id varchar(300),
      company_web varchar(50),
      industry varchar(50),
      pub_date TIMESTAMP DEFAULT now(),
      describe text,
      PRIMARY KEY (id)
)""")


class JobadPipeline(object):
    def __init__(self, dbpool):
        self.dbpool = dbpool

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        # Connect string for postgre sql
        kw = dict(
            host=settings.get('DB_HOST',' localhost'),
            port=settings.get('DB_PORT', 5432),
            user=settings.get('DB_USER', 'postgres'),
            database=settings.get('DB_NAME', 'road_network'),
            password=settings.get('DB_PASSWD'),
            client_encoding = 'UTF8',
        )
        # Create table in database
        import psycopg2
        conn = psycopg2.connect('dbname={database} user={user} password={password} host={host} port={port}'.format(**kw))
        cursor = conn.cursor()
        cursor.execute(CREATE_JOB_TABLE_SQL)
        conn.commit()
        conn.close()
        dbpool = adbapi.ConnectionPool('psycopg2', **kw)
        return cls(dbpool)

    def process_item(self, item, spider):
        if spider.name == "zhilian":
            if not item.get("work_place",""):
                raise DropItem("未提取到工作地址:%s" % item['job_title'])
            if "人" in item.get("position_count", "1"):
                position_count = item["position_count"]
                i = position_count.replace("人", "")
                item["position_count"] = int(i)
            else:
                pipe_log("[Unusual format position_count]" + str(item["position_count"]))
                item["position_count"] = 1
            if not item.get("salary_ave", ""):
                item["salary_ave"] = -1
            d = self.dbpool.runInteraction(self._do_execute, item, spider)
            d.addErrback(self._handle_error, item, spider)
            d.addBoth(lambda _:item)
            return d
        else:
          return item

    def _do_execute(self, conn, item, spider):
        if spider.name == "zhilian":
            for key in item:
                if isinstance(item[key], str):
                    item[key] = item[key].strip()
            conn.execute("select 1 from jobad where url=%s",(item['url'],))
            if conn.fetchall():
                raise DropItem("重复链接:%s" % item['url'])
            try:
                conn.execute(
                        """insert into jobad (source, url, job_title, job_category, position_count,
                            work_place, salary_range, salary_ave, area, edu, experience, company_name,
                            company_loc, company_property, company_online_id, company_web, industry,
                            pub_date, describe)  
                        values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                            (item["source"], item["url"], item["job_title"],item["job_category"],str(item["position_count"]),
                            item["work_place"],item["salary_range"],str(item["salary_ave"]),item["area"],item["edu"],item["experience"],item["company_name"],
                            item["company_loc"], item["company_property"], item["company_online_id"], item.get("company_web", "NULL"), item["industry"],
                            item["pub_date"], item["describe"]
                            )
                 )
            except Exception as ex:
                logging.error("zhilian database execute error:" + str(ex) +"\n")

    def _handle_error(self, failure, item, spider):
        logging.error(failure)
