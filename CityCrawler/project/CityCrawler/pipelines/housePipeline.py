# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import logging
from scrapy.exceptions import DropItem
from twisted.enterprise import adbapi


PIPE_LOG = open("housePipeline.log", "w")
def pipe_log(info):
    PIPE_LOG.write(str(info) + "\n")


CREATE_TABLE_SQL = (
"""CREATE TABLE IF NOT EXISTS realty (
      id BIGSERIAL,
      url varchar(100) UNIQUE NOT NULL,
      source varchar(20),
      title varchar(150) DEFAULT NULL,
      city varchar(10) DEFAULT NULL,
      price REAL NOT NULL,
      unit_price REAL DEFAULT NULL,
      address varchar(300),
      community varchar(50),
      space REAL NOT NULL,
      layout varchar(50),
      floor varchar(50),
      gps geometry(Point,4326) DEFAULT NULL,
      build_year smallint,
      pub_date TIMESTAMP DEFAULT now(),
      history_price TEXT DEFAULT NULL, 
      traffic varchar(50) DEFAULT NULL,
      basic_info text,
      PRIMARY KEY (id)
)""")

#CREATE INDEX staypointgps ON staypoint USING GIST ( gps );

class HousePipeline(object):
    def __init__(self, dbpool):
        self.dbpool = dbpool

    @classmethod
    def from_crawler(cls, crawler):
        print("here")
        settings = crawler.settings
        # Connect string for postgre sql
        kw = dict(
            host=settings.get('DB_HOST',' localhost'),
            port=settings.get('DB_PORT', 5432),
            user=settings.get('DB_USER', 'postgres'),
            database=settings.get('DB_NAME', 'road_network'),
            password=settings.get('DB_PASSWD'),
            client_encoding = 'UTF8',
        )
        # Create table in database
        import psycopg2
        conn = psycopg2.connect('dbname={database} user={user} password={password} host={host} port={port}'.format(**kw))
        cursor = conn.cursor()
        cursor.execute(CREATE_TABLE_SQL)
        conn.commit()
        conn.close()
        dbpool = adbapi.ConnectionPool('psycopg2', **kw)
        return cls(dbpool)
 

    def process_item(self, item, spider):
        if spider.name == "ershoufanglianjia":
            if not item.get("housePrice",""):
                raise DropItem("未提取到房价信息:%s" % item['url'])
            else:
                item["housePrice"] = float(item["housePrice"])
            # 经纬度数据强制类型转换
            try:
                item["baiduLongitude"] = float(item["baiduLongitude"])
                item["baiduLatitude"] = float(item["baiduLatitude"])
            except Exception as ex:
                logging.error("Format error for latitude/longitude ", item["baiduLatitude"], item["baiduLongitude"])
            # 格式化房屋面积数据
            if "平" in item.get("floorSpace", "-1"):
                floor_space = item["floorSpace"]
                i = floor_space.replace("平", "")
                item["floorSpace"] = float(i)
            else:
                pipe_log("[Unusual format floorSpace]" + str(item["floorSpace"]))
                item["floorSpace"] = 1
            if "年" in item["buildYear"]:
                year, _ = item["buildYear"].split("年")
                item["buildYear"] = int(year)
            d = self.dbpool.runInteraction(self._do_execute, item, spider)
            d.addErrback(self._handle_error, item, spider)
            d.addBoth(lambda _:item)
            return d
        else:
          return item

    def _do_execute(self, conn, item, spider):
        if spider.name == "ershoufangLianjia":
            # strip 所有字符串
            for key in item:
                if isinstance(item[key], str):
                    item[key] = item[key].strip()
            # 判重，未更新
            conn.execute("select 1 from realty where url=%s",(item['url'],))
            if conn.fetchall():
                raise DropItem("重复链接:%s" % item['url'])
            try:
                conn.execute(
                        """insert into realty (source, url, title, city, price,
                            unit_price, address, community, space, layout, floor, gps,
                            build_year, traffic, basic_info)  
                        values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ST_GeomFromText('POINT(%s %s)', 4326), %s, %s, %s)""",
                            (item["source"], item["url"], item["houseTitle"],item["houseCity"],str(item["housePrice"]),
                            item["houseAvePrice"],item["houseAddress"],str(item["houseCommunity"]),item["floorSpace"],item["layout"],item["floor"],
                            item["baiduLongitude"],item["baiduLatitude"],
                            item["buildYear"], item["traffic"], item["basicInfo"]
                            )
                 )
            except Exception as ex:
                logging.error("%s database execute error:"%spider.name + str(ex) +"\n")

    def _handle_error(self, failure, item, spider):
        logging.error(failure)