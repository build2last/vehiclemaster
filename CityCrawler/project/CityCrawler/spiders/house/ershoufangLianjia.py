# coding:utf-8

import scrapy
from CityCrawler.items import HouseItem

import logging
logging.basicConfig(level=logging.INFO,
                format='%(message)s',
                datefmt='%a, %d %b %Y %H:%M:%S',
                filename='链家爬虫.log',
                filemode='w')

class ershoufangLianjia(scrapy.Spider):
    name = 'ershoufanglianjia'
    allowed_domains = ['lianjia.com']
    start_urls = ["http://sh.lianjia.com/ershoufang/d%d"%i for i in range(1,2)]#startURL.ershoufangURL
    base_url = "http://sh.lianjia.com"
    custom_settings = {
            #"DOWNLOAD_DELAY":0.5,
            #"LOG_LEVEL":"DEBUG",
        }
    def parse(self, response):
        url_suffix = response.xpath('//div[@class="prop-title"]/a/@href').extract()
        urls = [self.base_url + url for url in url_suffix]
        for house_page_url in urls:
            yield scrapy.Request(house_page_url,callback=self.parse_house_page)
        try:
            next_page = self.base_url + response.xpath('//a[@gahref="results_next_page"]/@href').extract_first()
            yield scrapy.Request(next_page, callback=self.parse)
        except Exception as e:
            print("Arrive the last navigation page!")

    def parse_house_page(self,response):
        item = HouseItem()
        item['url'] = response.url
        item['source'] = "链家二手房"
        item['houseTitle'] = response.xpath('//html/head/title/text()').extract()[0]
        item['houseCity'] = response.url.split(".")[0].split("/")[-1]
        item['housePrice'] = response.xpath("//div[@class='price-total']/span[@class='price-num']/text()").extract_first()
        item['houseAvePrice'] = response.xpath("//p[@class='price-unit-num']/span/text()").extract_first()
        item['baiduLongitude'] = response.xpath('//div[@id="aroundApp"]/@longitude').extract_first()
        item['baiduLatitude'] = response.xpath('//div[@id="aroundApp"]/@latitude').extract_first()
        try:
            item['houseAddress'] = response.xpath("//span[@class='item-cell maininfo-estate-address']/text()").extract_first()
            item['houseCommunity'] = ",".join(response.xpath('//span[@class="maininfo-estate-name"]//a/text()').extract())
            item['floorSpace'] = response.xpath('//li[@class="main-item u-tr"]/p[1]/text()').extract_first()
            item['layout'] = ",".join([i.strip() for i in response.xpath('//li[@class="main-item"]//p/text()').extract()])
            item['floor'] = ",".join([i.strip() for i in response.xpath('//li[@class="main-item u-tc"]//p/text()').extract()])
            item['buildYear'] = response.xpath('//li[@class="main-item u-tr"]/p[2]/text()').extract_first().strip() 
            item['traffic'] = response.xpath('//ul[@class="maininfo-minor maininfo-item"]/li[3]/span[2]/text()').extract_first()
            item['basicInfo'] = response.xpath('//div[@class="content-main module-tb"]').extract_first()
        except Exception as ex:
            logging.info(ex)
        yield item