# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import hashlib
from six.moves import urllib

import logging
# logging.basicConfig(level=logging.INFO,
#                 format='%(message)s',
#                 datefmt='%a, %d %b %Y %H:%M:%S',
#                 filename='智联招聘爬虫.log',
#                 filemode='w')

import scrapy
from CityCrawler.items import JobadItem

"""
抓取上海市全职职位招聘信息
"""
class ZhilianSpider(scrapy.Spider):
    name = 'zhilian'
    allowed_domains = ['zhaopin.com']
    city = "上海"
    start_urls = ['https://sou.zhaopin.com/jobs/searchresult.ashx?jl='+urllib.parse.quote(city)]

    def parse(self, response):
        news = response.xpath('//div[@class="newlist_list_content"]//table[@class="newlist"]')
        job_urls = news.xpath('.//td[@class="zwmc"]//div/a/@href').extract()
        for job_url in job_urls:
            if "jobs.zhaopin.com" not in job_url:
                continue
            yield scrapy.Request(job_url, callback=self.parse_job_contents)
        next_page_url = response.xpath('//a[@class="next-page"]/@href').extract_first()
        yield scrapy.Request(next_page_url, callback=self.parse)

    def parse_job_contents(self, response):
        item = JobadItem()
        item["source"] = "智联招聘"
        item["url"] = response.url
        job_salary_range = response.xpath('//ul[@class="terminal-ul clearfix"]/li[1]/strong/text()').extract_first().strip()
        job_title = response.xpath('//div[@class="fixed-inner-box"]/div[1]/h1[1]/text()').extract_first()
        position_count = response.xpath("/html/body/div[6]/div[1]/ul/li[7]/strong/text()").extract_first()
        area = "".join(response.xpath('/html/body/div[6]/div[1]/ul/li[2]/strong//text()').extract())
        experience = response.xpath('/html/body/div[6]/div[1]/ul/li[5]/strong/text()').extract_first()
        edu = response.xpath('//ul[@class="terminal-ul clearfix"]/li[6]/strong/text()').extract_first()
        pub_date = response.xpath('//ul[@class="terminal-ul clearfix"]/li[3]/strong//text()').extract_first()
        job_category = response.xpath('//ul[@class="terminal-ul clearfix"]/li[8]/strong//text()').extract_first()
        describe = "\n".join(response.xpath('//div[@class="terminalpage-main clearfix"]//div[@class="tab-inner-cont"][1]//text()').extract())
        work_place = response.xpath('//div[@class="tab-inner-cont"]/h2[1]/text()').extract_first()
        if not work_place:
            strings = response.xpath('//div[@class="tab-inner-cont"]//h2/text()').extract()
            for s in strings:
                if "路" in s:
                    work_place = s.strip()
        company_name = response.xpath('/html/body/div[6]/div[2]/div[1]/p/a/text()').extract_first()
        company_box_content = "".join(response.xpath('//div[@class="company-box"]//text()').extract())
        if "公司主页" in company_box_content:
            company_link = response.xpath('/html/body/div[6]/div[2]/div[1]/ul/li[4]/strong/a/text()').extract_first()
            item["company_web"] = company_link
            company_loc = response.xpath('//div[@class="company-box"]/ul/li[5]/strong//text()').extract_first()
        else:
            company_loc = response.xpath('/html/body/div[6]/div[2]/div[1]/ul/li[4]/strong/text()').extract_first()
        id_link = response.xpath('//p[@class="company-name-t"]/a[1]/@href').extract_first()
        company_property = response.xpath('/html/body/div[6]/div[2]/div[1]/ul/li[2]/strong/text()').extract_first()
        industry = response.xpath('/html/body/div[6]/div[2]/div[1]/ul/li[3]/strong/a/text()').extract_first()   
        item["company_online_id"] = id_link
        item["job_title"] = job_title
        item["job_category"] = job_category
        item["position_count"] = position_count
        item["work_place"] = work_place
        item["salary_range"] = job_salary_range
        # 处理工资收入时应对 “工资面议” 等意外格式
        try:
            money_str = job_salary_range.split("元")[0]
            salary_ave = sum([int(i) for i in money_str.split("-")])/2
            item["salary_ave"] = salary_ave
        except Exception as e:
            logging.info("Format unusual:"+job_salary_range)
        item["area"] = area
        item["edu"] = edu
        item["experience"] = experience
        item["company_name"] = company_name
        item["company_loc"] = company_loc.strip()
        item["company_property"] = company_property
        item["industry"] = industry
        item["pub_date"] = pub_date
        item["describe"] = describe.strip()
        yield item