# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class HouseItem(scrapy.Item):
    # Based on LianJia
    houseTitle = scrapy.Field()             # 标题
    houseCity = scrapy.Field()              # 城市
    housePrice = scrapy.Field()             # 总价
    houseAvePrice = scrapy.Field()          # 均价
    houseAddress = scrapy.Field()           # 地址   
    baiduLongitude = scrapy.Field()         # 经度
    baiduLatitude = scrapy.Field()          # 纬度
    houseCommunity = scrapy.Field()         # 社区
    floorSpace = scrapy.Field()             # 使用面积
    layout = scrapy.Field()                 # 布局
    floor = scrapy.Field()                  # 楼层与朝向
    buildYear = scrapy.Field()              # 建成时间
    pubTime = scrapy.Field()                # 发布时间
    houseHistoryPrice = scrapy.Field()      # 历史价格
    traffic = scrapy.Field()                # 交通
    basicInfo = scrapy.Field()              # 基本信息（原始HTML文本）
    url = scrapy.Field()
    source = scrapy.Field()


class JobadItem(scrapy.Item):
    """
    从招聘网站获取的职位信息

    1. 网站代号：表明信息来源
    2. 职位名称
    3. 职位类别
    4. 岗位数量
    5. 工作地
    6. 收入范围
    7. 平均收入
    8. 工作地所在区域
    9. 教育要求
    10. 工作经验要求
    11. 公司名称
    12. 公司注册地
    13. 公司资本属性
    14. 公司id：网站内公司的id
    15. 公司所属行业
    16. 发布日期
    17. 岗位描述
    """
    source = scrapy.Field()
    url = scrapy.Field()
    job_title = scrapy.Field()
    job_category = scrapy.Field()
    position_count = scrapy.Field()
    work_place = scrapy.Field()
    salary_range = scrapy.Field()
    salary_ave = scrapy.Field()
    area = scrapy.Field()
    edu  = scrapy.Field()
    experience = scrapy.Field()
    company_name = scrapy.Field()
    company_loc = scrapy.Field()
    company_property = scrapy.Field()
    company_online_id = scrapy.Field()
    company_web = scrapy.Field()
    industry = scrapy.Field()
    pub_date = scrapy.Field()
    describe = scrapy.Field()