# 在售房屋信息爬虫

抓取在售房屋的价格、均价、位置等信息，目的是希望能得到城市不同区域的房屋市场和价格信息。

目标网站是中国市场上的网上中介平台。

## 数据源选择标准
1. 真实；
2. 信息全面；
3. 数据量大；
4. 数据持续更新；
5. 抓取难度。

## 信息来源网站
1. 链家：据说最为真实，单次可抓取的量为100个网页共3000条数据。