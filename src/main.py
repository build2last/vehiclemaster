#coding:utf-8

from Model.Models import StayPoint
import conf
from conf import *
from conf import DRIVING_DATA_PATH, POSTGIS_CON_STR, OUTPUT_STAY_POINT_DIR, POI_PATH
from conf import POSTGIS_CON_STR
from DataProcess import StayPointExtractor as SP
from DataProcess import UserPOIExtractor as upe
from DataProcess import FeatureMaker as FM
from DataAnalysis import LDA_POI_model as lda_poi

def extract_stay_point():
	drive_data_file_paths = filter(lambda x:x.endswith("txt"), SP.read_file_paths_from_dir(DRIVING_DATA_PATH))
	for path in drive_data_file_paths:
		SP.stay_point_detect(path, to_dir=OUTPUT_STAY_POINT_DIR)

def load_stay_points_to_database():
	SP.dir2db(OUTPUT_STAY_POINT_DIR, POSTGIS_CON_STR)

def generate_user_poi_doc():
	file_pathes = SP.read_file_paths_from_dir(OUTPUT_STAY_POINT_DIR, extension="StayPoint")
	for p in file_pathes:
		stay_point_items = StayPoint.load(p)
		upe.knn_extract(POI_PATH, stay_point_items, conf.OUTPUT_USER_POI_DIR)

def prepare_feature():
	for l in FM.dump_feature_vector_list():
		print(l)

def run_user_poi_topic_lda_model():
	lda_poi.run_lda(words_list=FM.load_word_list())


if __name__ == '__main__':
	import time
	tick = time.time()
	#extract_stay_point()
	#load_stay_points_to_database()
	#generate_user_poi_doc()
	run_user_poi_topic_lda_model()
	tock = time.time()
	print("-------\ntime cost: %fs"%(tock - tick))
