#coding:utf-8

"""
希望对采集到的原始汽车数据有更多了解，包括：
1. 数据采集间隔时间；
2. 每个停留点停留多长时间；
3. 每个停留点在预计范围内能得到多少设施标签；
"""

"""
时间	经度	纬度	停留时间#poi种类id
例如：2015-10-29 14:06:09	121326850	31219510	2#13;13;21;21;
"""
import numpy as np
import matplotlib.pyplot as plt

from Model.Models import GPS
from Model.Models import TraceLogDataItem

def time_delta_statistic(data_file_path_list):
	"""Plot the bar gragh of time delta between trace log.
	
	doc:https://matplotlib.org/api/_as_gen/matplotlib.pyplot.bar.html#matplotlib.pyplot.bar
	"""
	delta_time = []
	for data_file_path in data_file_path_list:
		with open(data_file_path) as fr:
			try:
				pre_line = fr.readline()
				fore_gps = GPS(pre_line)
				for line in fr:
					current_gps = GPS(line)
					time_delta = (current_gps.time - fore_gps.time).seconds
					delta_time.append(time_delta)
			except Exception as e:
				print(e)
	delta_time = np.array(delta_time, dtype=float)
	#delta_time = delta_time[delta_time<180]
	#gap = 10
	gap = 10000
	bar_number = (int(np.max(delta_time))//gap) + 1
	X = [i*gap for i in range(1, bar_number+1)]
	Y = [0]*bar_number
	for i,upper_bound in enumerate(X):
			Y[i] += np.sum(delta_time <= upper_bound)
			if i >0:
				Y[i] -= Y[i-1]
	plt.bar(X, Y, width=gap/1.5, align='center')
	plt.show()


def mileage_ascend_check(data_file_path_list):
	"""Check whether the mileage is always ascending"""
	for data_file_path in data_file_path_list:
		counter_total = 0
		error_count = 0
		with open(data_file_path) as fr:
			first_line = fr.readline()
			pre_mi = TraceLogDataItem(first_line).total_mileage
			try:
				for line in fr:
					counter_total += 1
					cu_mi = TraceLogDataItem(line).total_mileage
					if cu_mi < pre_mi:
						error_count += 1
					pre_mi = cu_mi
			except Exception as e:
				print(e)
		print("%d/%d Error log found in %s"%(error_count, counter_total,data_file_path))

def speed_data_check(data_file_path_list):
	for data_file_path in data_file_path_list:
		counter_total = 0
		error_count = 0
		with open(data_file_path) as fr:
			try:
				for line in fr:
					l = TraceLogDataItem(line)
					if l.speed > 0 and l.status != "1":
						error_count += 1
					counter_total += 1
			except Exception as e:
				print(data_file_path, e)			
		print("[speed_data_check] %d/%d Error log found in %s"%(error_count, counter_total,data_file_path))


