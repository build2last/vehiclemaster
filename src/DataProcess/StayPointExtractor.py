#coding:utf-8

# Python 2/3 compatible
from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals
"""
提取停留点和充电地点以及停留时间

基本假设：
1. 充电时得到的记录包含最后熄火时间，但最后启动时间缺失；行驶时的记录包含最后启动时间，但最后熄火时间缺失；
2. 数据条目按照时间顺序记录;
3. 30分钟里程变动不足2km的，根据GPS坐标去重后，同样认为在此处获得一个停留点。

对于不满足以上条件的记录，认为无法获得停留时间。
"""


import os
import time
import datetime
import logging

from conf import *
from Model.Models import TraceLogDataItem, StayPoint


def stay_point_detect(file_path, to_dir, to_file="1"):
	CAR_START = "1"
	counter = 0
	if to_file:
		to_file = os.path.join(to_dir, os.path.basename(file_path) + ".StayPoint")
	staypoints = []
	with open(file_path) as fr:
		pre_line = fr.readline()
		current_line = fr.readline()
		next_line = fr.readline()
		while next_line and current_line:
			v1 = pre_line.strip().split(",")
			v2 = current_line.strip().split(",")
			sta1 = v1[STATUS]
			sta2 = v2[STATUS]
			if sta1.strip() == CAR_START and sta2.strip() != CAR_START: # Stop the car
				try:
					stay_p = TraceLogDataItem(current_line)
					staypoints.append(stay_p)
					counter += 1
				except Exception as e:
					print(e, current_line)
			else:
				m1 = v1[MILEAGE]
				m2 = v2[MILEAGE]
				ctime1 = time.strptime(v1[COLLECTTIME], "%Y-%m-%d %H:%M:%S")
				ctime2 = time.strptime(v2[COLLECTTIME], "%Y-%m-%d %H:%M:%S")
				period = (datetime.datetime(*ctime2[:6]) - datetime.datetime(*ctime1[:6])).seconds
				m = float(m2) - float(m1)
				if period/60 > 30 and m < 2 and sta1 == CAR_START:
					stay_p = TraceLogDataItem(current_line)
					if staypoints:
						# Compare GPS to remove duplicate stay point for stay at the same place.
						if not (staypoints[-1].longitude == stay_p.longitude and staypoints[-1].latitude == stay_p.latitude):
							staypoints.append(stay_p)
							counter += 1		
			if sta1.strip() != CAR_START and sta2.strip() == CAR_START: # Start the car
				if staypoints:
					item = TraceLogDataItem(current_line)
					if item.start_time:
						staypoints[-1].staytime = (item.collect_time - item.start_time).seconds//60
			pre_line = current_line
			current_line = next_line
			next_line = fr.readline()
	print("Detected %d Stay Points in %s"%(counter, file_path))
	with open(to_file, "w") as fw:
		content = ""
		for p in staypoints:
			#ToVerify： Will it be faster to write all content for one time, without regard to memory cost?
			content += p.as_stay_point_str()+"\n"
		fw.write(content)
	return 1


def charging_point_detect(file_path, to_dir, to_file="1"):
	# Charging status code is "2"
	TARGET_STATUS = "2"
	counter = 0
	if to_file:
		to_file = os.path.join(to_dir, os.path.basename(file_path) + ".ChargePoint")
	points = []
	with open(file_path) as fr:
		pre_line = fr.readline()
		current_line = fr.readline()
		next_line = fr.readline()
		while next_line and current_line:
			v1 = pre_line.strip().split(",")
			v2 = current_line.strip().split(",")
			sta1 = v1[STATUS]
			sta2 = v2[STATUS]
			if sta1.strip() != TARGET_STATUS and sta2.strip() == TARGET_STATUS:
				try:
					stay_p = TraceLogDataItem(current_line)
					points.append(stay_p)
					counter += 1
				except Exception as e:
					print(e, current_line)		
			if sta1.strip() == TARGET_STATUS and sta2.strip() != TARGET_STATUS:
				item = TraceLogDataItem(current_line)
				if points:
					if item.end_time:
						points[-1].staytime = (item.collect_time - item.end_time).seconds//60
			pre_line = current_line
			current_line = next_line
			next_line = fr.readline()
	print("Detected %d Charging Points in %s"%(counter, file_path))
	with open(to_file, "w") as fw:
		content = ""
		for p in points:
			#ToVerify： Will it be faster to write all content for one time, without regard to memory cost?
			content += p.as_stay_point_str()+"\n"
		fw.write(content)
	return 1


def load_stay_points_from(file_path):
	"""Read text file - Data abstract function

	input:  Stay points data file path
	return: StayPoint objects
	"""
	s_points = []
	with open(file_path) as fr:
		for line in fr:
			try:
				s_points.append(StayPoint(line))
			except UnicodeDecodeError as ue:
				print(ue)
				print(line)
	return s_points

def load_stay_points_in_dir(dir_path):
	file_paths = read_file_paths_from_dir(dir_path, extension=(".StayPoint", "ChargePoint"))
	items = {}
	for p in file_paths:
		carid = os.path.split(p)[-1].split(".")[0]
		items[carid] = load_stay_points_from(p)
	return items



"""----------------------------------------
Database opeartion from now on!

1. Create table in database;
2. Load data to database
"""

import psycopg2

def create_staypoints_table(connect_string):
	#stay point table： id SERIAL primary key, carid, time, mileage, GPS, STATUS
	#TODO(Liu Kun) Current greenplum not support "IF NOT EXISTS" clause, alternative SQL may help check 
	#table before createt table like:
	"""
		SELECT EXISTS (
		   SELECT 1 
		   FROM   pg_tables
		   WHERE  schemaname = 'schema_name'
		   AND    tablename = 'table_name'
		   );
	"""
	CREATE_TABLE_SQL = """CREATE TABLE staypoint( 
		id serial primary key, 
		carid varchar(30),
		time timestamp,
		staytime int,
		status smallint
	);
	"""
	ADD_2DPOINT_COLUMN_IN_TABLE_SQL = "SELECT AddGeometryColumn ('staypoint', 'gps', 4326, 'POINT', 2);"
	try:
		conn = psycopg2.connect(connect_string)
		cursor = conn.cursor()
		cursor.execute(CREATE_TABLE_SQL)
		cursor.execute(ADD_2DPOINT_COLUMN_IN_TABLE_SQL)
		conn.commit()
		conn.close()
	except Exception as e:
		print(e)
		return 0

	print("Create table [staypoint] successful!")
	return 1


def dump_items_to_db(stay_point_items, connect_string, table_name="vehicle_staypoint"):
	conn = psycopg2.connect(connect_string)
	cursor = conn.cursor()
	for carid in stay_point_items:
		s_points = stay_point_items[carid]
		for point in s_points:
			item = point.as_dic()
			cursor.execute("""INSERT INTO {table_name} (carid, time, staytime, status, gps) 
				VALUES ('{carid}', '{time}', {staytime}, {status}, 
				ST_GeomFromText('POINT({longitude} {latitude})',4326));""".format(
					table_name = table_name,
					carid=carid, time=item["time"], status=item["status"],
					staytime=item["staytime"], longitude=item["longitude"], latitude=item["latitude"])
				)
		conn.commit()
	conn.close()
	return 1


def dir2db(dir_path, db_string, table_name, db_func=dump_items_to_db):
	items = load_stay_points_in_dir(dir_path)
	db_func(items, db_string, table_name)
	print(u"停留点数据载入数据库完成")
	return 1