#coding:utf-8
"""
制作特征模型，提供给分析模块进行较为复杂的机器学习
"""

import collections
import logging
import os
import conf
from Model.Models import POI

"""------------------------------------------------------
LDA 用户兴趣主题模型建模

for LDA_POI_model.py
"""
import pickle

#POI document features in Bag of words matrix
def load_word_list(vector_source_file=conf.POI_CATAGORY_PATH):
    with open(vector_source_file) as fr:
        return fr.read().split(u"、")

def dump_feature_vector_list(poi_doc_dir=conf.OUTPUT_USER_POI_DIR):
    POI_CAT_NUM = len(open(conf.POI_CATAGORY_PATH).read().split("、"))
    user_poi_files = map(lambda filename:os.path.join(poi_doc_dir, filename), 
        filter(lambda x:x.endswith(".UserDoc"), os.listdir(poi_doc_dir)))
    users_matrix = []
    car_ids = []
    for file in user_poi_files:
        user_feature_vector = [0] * POI_CAT_NUM
        p_items = POI.load(file)
        car_ids.append(p_items["id"])
        tags = []
        for p in p_items["points"]:
            for tag in p.tags:
                user_feature_vector[tag.id-1] += 1
        users_matrix.append(user_feature_vector)
    with open(conf.POI_FEATURE_MATRIX+".carid", "w") as fw:
        for car_id in car_ids:
            fw.write(car_id+"\n")
    with open(conf.POI_FEATURE_MATRIX, "wb") as fw:
        pickle.dump(users_matrix, fw)
    return 1


# def load_word_doc_to_BOW(doc_file):
#     tag_bag = []
#     with open(doc_file) as fr:
#         for line in fr:
#             tags = fr.split(",")[-1]
# #生成词袋模型特征向量
# def generate_doc_vector(words_bag, words_list):
#     words_counter_dic = collections.defaultdict(int)    #指定字典的值为列表
#     for words in words_bag:
#         if words in words_list:
#             words_counter_dic[words] += 1
#     vec = [words_counter_dic[words_list[i]] for i in range(len(words_list))]
#     del words_counter_dic
#     return vec
# def dump_feature_vector_list(poi_doc_dir=conf.OUTPUT_USER_POI_DIR):
#     import pickle
#     user_poi_files = filter(lambda x:x.endswith(".UserDoc"), os.listdir(poi_doc_dir))
#     feature_vector_list = []
#     for poi_file in user_poi_files:
#             time_tags_hash_table = collections.defaultdict(list)
#             user_poi_list = []
#             path = os.path.join(poi_doc_dir, poi_file)
#             points = POI.load(POI)
#             tags = []
#             for p in points:
#                 tags = p.tags

    #         with open(os.path.join(poi_doc_dir, poi_file)) as fr:
    #             try:
    #                 for line in fr:
    #                     line = line.strip()
    #                     loc, time_str, name, tag_str = facility_name = line.split("")
    #                     tags = tag_str.split(";")
    #                     if name not in time_tags_hash_table[time_str]:
    #                         time_tags_hash_table[time_str].append(name)
    #                         user_poi_list.extend(tags)
    #             except Exception as e:
    #                 print(poi_file)
    #                 logging.error(e)
    #                 continue
    #         words_bag = user_poi_list
    #         words_list = load_word_list()
    #         feature_vector_list.append(generate_doc_vector(words_bag, words_list))
    # with open(conf.POI_FEATURE_MATRIX, "wb") as fw:
    #     pickle.dump(feature_vector_list, fw)
    # return feature_vector_list