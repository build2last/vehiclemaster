#coding:utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

# Liu Kun start writting these code in 2018-01 in Shanghai as founder.
__LATEST_EDIT__ = "2017-12-24"

import logging
import os
import conf
from Model.Models import haversine
from Model.Models import TraceLogDataItem
from Model.Models import haversine
"""
用KNN算法提取用户兴趣点；

Extract user POI with KNN.
Distance constrain is setted!
"""

def knn_extract(poi_file, stay_point_items, toDir, neighbor_number=15, top_k=11, distance_limit=1000):
    """POI match with KNN

    # KNN to find the matching tags with 1000 meters distance limit
    # Filter the POI out of the bound
    # Generate POI doc for each car
    """
    import numpy as np
    from sklearn.model_selection import train_test_split
    from sklearn.neighbors import KNeighborsClassifier
    counter_tags = 0
    car_id = stay_point_items["id"]
    X_data = []
    Y_data = []
    tags_data = []
    with open(poi_file) as fr:
        for line in fr:
            values = line.strip().split(',')
            X_data.append([float(value) for value in values[-4:-2]])
            Y_data.append(values[-1])
            tags_data.append(values[-2])
    x = np.array(X_data)
    y = np.array(Y_data)
    knn_model = KNeighborsClassifier(n_neighbors=neighbor_number)
    knn_model.fit(x, y)
    pipe_to_file = os.path.join(toDir, stay_point_items["id"]+".UserDoc")
    with open(pipe_to_file, "wb") as fw:
        for point in stay_point_items["points"]:
            testing_x = [point.GPS.latitude, point.GPS.longitude]
            distances, point_indices = knn_model.kneighbors([testing_x], top_k)
            point_indices = list(point_indices.flat)
            tags = []
            for i in range(len(point_indices)):
                lat1, lon1 = x[point_indices[i]]
                lat2, lon2 = testing_x
                haversine_distance = haversine(lat1,lon1,lat2,lon2)
                if haversine_distance < distance_limit:
                    tags.append(y[point_indices[i]])
                    counter_tags += 1
            fw.write((point.to_str() + "\t" + ";".join(tags) + "\n").encode("utf-8"))
    print("[knn_extract] Detected %d POI tags for %d STAY POINTS in %s "%(counter_tags, len(stay_point_items["points"]), poi_file))
    return 1


"""---------------------------------
将从网络上获取的POI地点数据导入到数据库中。
动机是：希望通过数据库建立的 GIS 索引加速后续对地点信息的查询。

Database operation

# Create table
# Add geomatry column
# dump data to database

!!!PostgreSQL + PostGIS extension is needed!!!
"""
import psycopg2

def create_POI_table(connect_string):
    CREATE_POI_TABLE_SQL = """CREATE TABLE poi( 
        id serial primary key, 
        name varchar(100),
        type varchar(100),
        type_id SMALLINT
    );
    """
    ADD_2DPOINT_COLUMN_IN_POI = "SELECT AddGeometryColumn ('poi', 'gps', 4326, 'POINT', 2);"
    try:
        conn = psycopg2.connect(connect_string)
        cursor = conn.cursor()
        cursor.execute(CREATE_POI_TABLE_SQL)
        cursor.execute(ADD_2DPOINT_COLUMN_IN_POI)
        conn.commit()
        conn.close()
        print("POI table created success!")
        return 1
    except Exception as e:
        print(e)
        return 0

def dump_data_to_database(file_path, connect_string):
    with open(file_path) as fr_location:
        conn = psycopg2.connect(connect_string)
        cursor = conn.cursor()
        for line in fr_location:
            try:
                values = line.strip().split(",")
                name = " ".join(values[0:-4]).replace("'", "")
                typ  = values[-2]
                type_id = int(values[-1].strip())
                latitude = float(values[-4])
                longitude = float(values[-3])
                cursor.execute("""INSERT INTO poi (name, type, type_id, gps) 
                    VALUES ('{name}', '{type}', '{type_id}', 
                    ST_GeomFromText('POINT({longitude} {latitude})',4326));""".format(
                        name=name, type=typ,
                        type_id=type_id, longitude=longitude, latitude=latitude)
                    )
            except Exception as e:
                print(line)
                print(e)
            conn.commit()
        conn.close()
    return 1