#coding:utf-8
from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

"""
Trace Extractor

提取行程轨迹
1. 从启动到停车之间的一段轨迹序列；
2. 连续轨迹的时间间隔上限是10min。
"""

import os
import datetime
import conf

from Model.Models import TraceLogDataItem

def trace_point_extract(file_path, to_dir):
	log_items = TraceLogDataItem.load(file_path)
	car_id = log_items["id"]
	to_file = os.path.join(to_dir, log_items["id"] + ".trace")
	if not log_items["points"]:
		return 0
	log_items = log_items["points"]
	start_time_temp = log_items[0].collect_time.strftime("%Y-%m-%d %H:%M:%S")
	counter_trace = 0
	with open(to_file, "w") as fw:
		fw.write("\t".join(["CollectTime","Longitude","Latitude","StartTime","TraceNumber"])+"\n")
		for item in log_items:
			if item.vehile_status != "1":
				continue
			start_time = item.start_time.strftime("%Y-%m-%d %H:%M:%S")
			if start_time != start_time_temp:
				counter_trace += 1
				start_time_temp = start_time
			str_format = "\t".join([item.collect_time.strftime("%Y-%m-%d %H:%M:%S"), 
									 		str(item.longitude), str(item.latitude), item.start_time.strftime("%Y-%m-%d %H:%M:%S"), str(counter_trace)])
			fw.write(str_format + "\n")


def trace_file_analyse(file_path, to_dir):
	trace_item = {}
	car_id = os.path.basename(file_path).split(".")[0]
	sta_file = os.path.join(to_dir, car_id + ".tracesta")
	fw = open(sta_file, "w")
	with open(file_path) as fr:
		title = fr.readline()
		trace_num_temp = 0
		fore_lat = .0
		fore_lng = .0
		for line in fr:
			values = line.strip().split("\t")
			collect_time = values[0]
			longitude = values[1]
			latitude = values[2]
			start_time = values[3]
			trace_num = int(values[4])
			if collect_time == start_time:
				print("Accurate start position!")
				trace_item[trace_num] = {"start_point":{}, "end_point":{}}
				trace_item[trace_num]["start_point"] = {"lat":latitude, "lng":longitude} 
			if trace_num != trace_num_temp:
				# 如果未被前面的条件语句精准匹配到，即轨迹未初始化
				if not trace_item.get(trace_num, ""):
					trace_item[trace_num] = {"start_point":{}, "end_point":{}}
					trace_item[trace_num]["start_point"] = {"lat":latitude, "lng":longitude} 
				if trace_num > 1:
					trace_item[trace_num-1]["end_point"] = {"lat":fore_lat, "lng":fore_lng}
			fore_lng = longitude
			fore_lat = latitude
			trace_num_temp = trace_num
		trace_item[trace_num]["end_point"] = {"lat":latitude, "lng":longitude} 
	for t_id in trace_item:
		try:
			line_str = (str(t_id)+"\t"+trace_item[t_id]["start_point"]["lat"] +"\t"+trace_item[t_id]["start_point"]["lng"] +
				"\t"+trace_item[t_id]["end_point"]["lat"] +"\t"+trace_item[t_id]["end_point"]["lng"])
			fw.write(line_str+"\n")
		except Exception as ex:
			print(ex)

