#coding:utf-8

from django.conf.urls import url
from .views import HomePageView, county_datasets, point_datasets
from .views import StayPoints, ChargePoints, Carview, JobPoints, HousePoints

urlpatterns = [
	url(r'^$', HomePageView.as_view(), name = 'home'),
	url(r'^car/(?P<carid>\w+)[/]$', Carview.as_view(), name="show_car"),
	url(r'^County_data/$', county_datasets, name = 'County'),
	url(r'^Incidence_data/$', point_datasets, name = 'Incidences'),
	url(r'^staypoint/(?P<carid>\w+)', StayPoints.as_view(), name='staypointcall'),
	url(r'^chargepoint/(?P<carid>\w+)', ChargePoints.as_view(), name='chargepointcall'),
	url(r'^jobpoint/(?P<area>\w+)', JobPoints.as_view(), name='jobpointcall'),
	url(r'^jobpoint[/]', JobPoints.as_view(), name='jobpointcall'),
	url(r'^houseprice[/]', HousePoints.as_view(), name='houseprice'),
]