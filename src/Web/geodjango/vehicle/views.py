# coding:utf-8
import logging
import datetime

import random
import numpy as np

from django.db import connection
from django.shortcuts import render
from django.shortcuts import get_list_or_404
from django.views.generic import TemplateView
from django.views.generic import View
from django.core.serializers import serialize
from django.http import HttpResponse
from .models import Counties,Incidences,StayPoint,ChargePoint
from django.db import connection, transaction

CAR_ID_VECTOR =['LGXC76C30E0082856', 'LGXC76C30E0141291', 'LGXC76C30F0116425', 'LGXC76C30F0126131', 'LGXC76C30F0137856', 'LGXC76C31E0185770', 'LGXC76C31F0096282', 'LGXC76C31F0105420', 'LGXC76C31F0144637', 'LGXC76C31F0146579', 'LGXC76C32E0121558', 'LGXC76C32E0130535', 'LGXC76C32E0182893', 'LGXC76C32E0214547', 'LGXC76C32F0116684', 'LGXC76C33E0184359', 'LGXC76C33F0074431', 'LGXC76C33F0099085', 'LGXC76C33F0105368', 'LGXC76C33F0122025', 'LGXC76C33F0128214', 'LGXC76C33F0132070', 'LGXC76C33F0140783', 'LGXC76C34E0047527', 'LGXC76C34F0138153', 'LGXC76C34F0150013', 'LGXC76C35E0099832', 'LGXC76C35E0120730', 'LGXC76C35E0145790', 'LGXC76C35E0214543', 'LGXC76C36E0099838', 'LGXC76C36E0144440', 'LGXC76C36E0162307', 'LGXC76C36E0168804', 'LGXC76C36F0020914', 'LGXC76C36F0050818', 'LGXC76C36F0116610', 'LGXC76C36F0131270', 'LGXC76C36F0148828', 'LGXC76C37F0130418', 'LGXC76C37F0132993', 'LGXC76C38E0126778', 'LGXC76C38E0140616', 'LGXC76C38E0157738', 'LGXC76C38F0009879', 'LGXC76C39F0097678', 'LGXC76C3XE0105656', 'LGXC76C3XE0183564', 'LGXC76C3XF0100264', 'LGXC76C3XF0157757']

# Create your views here.
class HomePageView(TemplateView):
    template_name = 'content/baiduMap.html'

def county_datasets(request):
    counties = serialize('geojson', Counties.objects.all())
    return HttpResponse(counties,content_type='json')

def point_datasets(request):
    points = serialize('geojson', Incidences.objects.all())
    return HttpResponse(points,content_type='json')

# Get an instance of a logger
logger = logging.getLogger(__name__)

# 停留点中心和工作点中心
CENTERS = {}
with open("vehicle/data/center.out") as fr:
    for line in fr:
        values = line.strip().split("\t")
        _id = values[0]
        stay_p_values = values[1].split(",")
        if len(stay_p_values) > 1:
            stay_p = [{"lat":float(p.split("-")[1]), "lng":float(p.split("-")[0])} for p in stay_p_values]
        if len(values)>2:
            charge_p_value = values[2]
            charge_p = {"lat":float(charge_p_value.split("-")[1]), "lng":float(charge_p_value.split("-")[0])}
        else:
            charge_p = ""
        CENTERS[_id] = {"stay":stay_p, "charge":charge_p}


"""
# -------------------
# 下面是一些地理计算方法
# 主要包括：
#  GPS椭球面距离计算
"""
from math import radians, cos, sin, asin, sqrt  
  
def haversine(p1, p2): 
    """ 
    Calculate the great circle distance between two points  
    on the earth (specified in decimal degrees) 

    The units are in meters
    """  
    lon1, lat1, lon2, lat2 = p1["lng"], p1["lat"], p2["lng"], p2["lat"]
    # 将十进制度数转化为弧度  
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])  
    # haversine公式  
    dlon = lon2 - lon1   
    dlat = lat2 - lat1   
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2  
    c = 2 * asin(sqrt(a))   
    r = 6371 # 地球平均半径，单位为公里  
    return c * r * 1000  


def load_traces(car_id):
    file_path = "vehicle/data/trace/%s.tracesta"%car_id
    items = []
    with open(file_path) as fr:
        for line in fr:
            values = line.strip().split("\t")
            start_point = {'lat':values[1], 'lng':values[2]}
            end_point = {'lat':values[3], 'lng':values[4]}
            items.append({"start_point":start_point, "end_point":end_point})
    return items


class Carview(View):
    def get(self, request, *args, **kwargs):
        car_id = kwargs.get("carid")
        trace_item = load_traces(car_id)
        print(trace_item)
        cursor = connection.cursor()
        stay_TIME_STA_SQL = "select date_part('hour',time) as shour,count(*) from vehicle_staypoint WHERE carid='%s' group by date_part('hour', time) ORDER BY shour;"%car_id
        charge_TIME_STA_SQL = "select date_part('hour',time) as shour,count(*) from vehicle_chargepoint WHERE carid='%s' group by date_part('hour', time) ORDER BY shour;"%car_id
        cursor.execute(stay_TIME_STA_SQL)
        stay_time_row = cursor.fetchall()
        x_values = []
        stay_time_values = []
        for i in stay_time_row:
            x_values.append(i[0])
            stay_time_values.append(i[1])
        cursor.execute(charge_TIME_STA_SQL)
        charge_time_row = cursor.fetchall()
        charge_time_values = []
        for i in charge_time_row:
            charge_time_values.append(i[1])
        center = CENTERS.get(car_id, {})
        live_point = center["charge"]
        if live_point:
            QUERY_SQL_TEMPLATE = "SELECT AVG(unit_price) FROM realty where ST_Distance(ST_GeogFromText('SRID=4326;POINT({lng} {lat})'), gps) < {dist} and unit_price is not null"
            cursor.execute(QUERY_SQL_TEMPLATE.format(lng=live_point["lng"], lat=live_point["lat"], dist=2000))
            row = cursor.fetchone()
            if row[0]:
                house_unit_price = int(row[0])
            else:
                house_unit_price = 0
        else:
            house_unit_price = 0

        work_point = ""
        work_points = center["stay"]
        if len(work_points) > 1 and live_point:
            p1 = work_points[0]
            p2 = work_points[1]
            if haversine(p1, live_point) > haversine(p2, live_point):
                work_point = p1
            else:
                work_point = p2
        if work_point:
            QUERY_SQL_TEMPLATE = "SELECT AVG(salary_ave) FROM jobad where ST_Distance(ST_GeogFromText('SRID=4326;POINT({lng} {lat})'), work_place_gps) < {dist} and work_place_gps is not null and salary_ave is not null"
            cursor.execute(QUERY_SQL_TEMPLATE.format(lng=work_point["lng"], lat=work_point["lat"], dist=2000))
            row = cursor.fetchone()
            if row:
                salary_ave = int(row[0])
            else:
                salary_ave = 0

        id_index = 0
        for i in range(len(CAR_ID_VECTOR)):
            if CAR_ID_VECTOR[i] == car_id:
                id_index = i
        simi_matrix = np.loadtxt("vehicle/data/living_center_similarity.out", dtype=np.float, delimiter=",")
        car_vec = np.array(CAR_ID_VECTOR)
        return render(request, 'content/baiduMap.html',
                      {
                          "carid": car_id,
                          "simicar": car_vec[simi_matrix[id_index]>0.98],
                          "staytime": stay_time_values,
                          "chargetime":charge_time_values,
                          "x":x_values,
                          "center":center,
                          "house_unit_price": house_unit_price,
                          "work_point":work_point,
                          "salary_ave":salary_ave,
                          "traces":trace_item
                      }
            )


class StayPoints(View):
    def get(self, request, *args, **kwargs):
        car_id = kwargs.get('carid', '')
        from_date = request.GET.get('fromdate','') 
        to_date = request.GET.get('todate','') 
        if not from_date:
            points = get_list_or_404(StayPoint, carid=car_id)
        else:
            try:
                from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d")
                points = points.filter(time__gte=from_date)
                if to_date:
                    to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d")
                    points = points.filter(time__lte=to_date)
            except ValueError as ve:
                print("Bad time format in url!")
                print(request.get_full_path())
        points_json = serialize('geojson', points, geometry_field='gps', 
            fields=['staytime',])
        return HttpResponse(points_json, content_type='json')


class ChargePoints(View):
    def get(self, request, *args, **kwargs):
        car_id = kwargs.get('carid', '')
        from_date = request.GET.get('fromdate','') 
        to_date = request.GET.get('todate','') 
        if not from_date:
            points = get_list_or_404(ChargePoint, carid=car_id)
        else:
            try:
                from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d")
                points = points.filter(time__gte=from_date)
                if to_date:
                    to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d")
                    points = points.filter(time__lte=to_date)
            except ValueError as ve:
                print("Bad time format in url!")
                print(request.get_full_path())
        points_json = serialize('geojson', points, geometry_field='gps', 
            fields=['staytime',])
        return HttpResponse(points_json, content_type='json')

import json
class JobPoints(View):
    def get(self, request, *args, **kwargs):
        QUERY_SQL_TEMPLATE = "SELECT ST_X(work_place_gps), ST_Y(work_place_gps), salary_ave FROM jobad where ST_Distance(ST_GeogFromText('SRID=4326;POINT({lng} {lat})'), work_place_gps) < {dist} and salary_ave is not null"
        area = kwargs.get('area', '')
        items = {"points":[]}
        content_json = "{miao!}"
        if area:
            pass
        else:
            try:
                lng = float(request.GET.get('lng',''))
                lat = float(request.GET.get('lat',''))
                dist = int(request.GET.get('dist','500000'))
            except Exception as float_ex:
                print(float_ex)
                return HttpResponse(content_json, content_type='json')
            if lng and lat:
                cursor = connection.cursor()
                cursor.execute(QUERY_SQL_TEMPLATE.format(lng=lng, lat=lat, dist=dist))
                rows = cursor.fetchall()
                # if len(rows) > 1000:
                #     rows = random.sample(rows, 1000)
                for row in rows:
                    lng, lat, salary = row
                    items["points"].append({'lng':lng,'lat':lat, 'salary':salary})
                items["lenght"] = len(items["points"])
                content_json = json.dumps(items)
        return HttpResponse(content_json, content_type='json')


class HousePoints(View):
    def get(self, request, *args, **kwargs):
        QUERY_SQL_TEMPLATE = "SELECT ST_X(gps), ST_Y(gps), unit_price FROM realty where ST_Distance(ST_GeogFromText('SRID=4326;POINT({lng} {lat})'), gps) < {dist} and unit_price is not null"
        area = kwargs.get('area', '')
        items = {"points":[]}
        content_json = "{miao!}"
        if area:
            pass
        else:
            try:
                lng = float(request.GET.get('lng',''))
                lat = float(request.GET.get('lat',''))
                dist = int(request.GET.get('dist','500000'))
            except Exception as float_ex:
                print(float_ex)
                return HttpResponse(content_json, content_type='json')
            if lng and lat:
                cursor = connection.cursor()
                cursor.execute(QUERY_SQL_TEMPLATE.format(lng=lng, lat=lat, dist=dist))
                rows = cursor.fetchall()
                # if len(rows) > 1000:
                #     rows = random.sample(rows, 1000)
                for row in rows:
                    lng, lat, unit_price = row
                    items["points"].append({'lng':lng,'lat':lat, 'unit_price':unit_price})
                items["lenght"] = len(items["points"])
                content_json = json.dumps(items)
        return HttpResponse(content_json, content_type='json')


