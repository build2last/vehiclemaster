#coding:utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
import conf
from DataProcess import FeatureMaker as FM 
import numpy as np

from conf import POI_FEATURE_MATRIX
"""
Refference: http://scikit-learn.org/dev/auto_examples/applications/plot_topics_extraction_with_nmf_lda.html#sphx-glr-auto-examples-applications-plot-topics-extraction-with-nmf-lda-py
"""

n_components = 10


def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message += " ".join([feature_names[i] + "(%f)"%topic[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        print(message)
    print()



def LDA_analyse(feature_vector_list, words_vector, n_topics=10, LDA_N_ITER=500):
    lda = LatentDirichletAllocation(max_iter=5,
                                    learning_method='online',
                                    learning_offset=1.,
                                    random_state=1,
                                    n_components=n_components)
    print(words_vector)
    topic_dist = lda.fit_transform(feature_vector_list)
    print(topic_dist)
    print_top_words(lda, words_vector, n_top_words=28)
    #图形展示
    import matplotlib.pyplot as plt  
    f, ax= plt.subplots(5, 1, figsize=(8, 6), sharex=True)  
    for i, k in enumerate([1, 3, 5, 7, 9]):  
        ax[i].stem(topic_dist[k,:], linefmt='r-',  
                   markerfmt='ro', basefmt='w-')  
        ax[i].set_xlim(-1, 21)  
        ax[i].set_ylim(0, 1)  
        ax[i].set_ylabel("Prob")  
        ax[i].set_title("Document {}".format(k))  
        ax[4].set_xlabel("Topic")  
    plt.tight_layout()  
    plt.show()  


def run_lda(words_list, poi_doc_pkl_path=conf.POI_FEATURE_MATRIX):
    import pickle
    with open(poi_doc_pkl_path, "rb") as fr:
        feature_matrix = pickle.load(fr)
    x_matrix = np.array(feature_matrix).astype(int, copy=False)
    f_matrix = feature_matrix
    LDA_analyse(f_matrix, words_list)
    return 1
