#coding:utf-8

"""
#Similarity between charging location and living locaton

# Python = 2.7
# Author : Liu Kun


#input : 	Text file encoded with utf-8 in csv format
#output:	Text file encoded with utff-8 in csv format
"""

import os
from math import radians, cos, sin, asin, sqrt, exp

import numpy as np


# 25 miles in meters
alpha = 40233

class Center:
	"""

	"""
	def __init__(self, line):
		values = line.strip().split(":")
		self.car_id = values[0].replace(".txt", "")
		longitude, latitude = values[-1].split(" ")
		self.longitude = float(longitude)
		self.latitude = float(latitude)

# Sorted car id
CAR_ID_VECTOR =['LGXC76C30E0082856', 'LGXC76C30E0141291', 'LGXC76C30F0116425', 'LGXC76C30F0126131', 'LGXC76C30F0137856', 'LGXC76C31E0185770', 'LGXC76C31F0096282', 'LGXC76C31F0105420', 'LGXC76C31F0144637', 'LGXC76C31F0146579', 'LGXC76C32E0121558', 'LGXC76C32E0130535', 'LGXC76C32E0182893', 'LGXC76C32E0214547', 'LGXC76C32F0116684', 'LGXC76C33E0184359', 'LGXC76C33F0074431', 'LGXC76C33F0099085', 'LGXC76C33F0105368', 'LGXC76C33F0122025', 'LGXC76C33F0128214', 'LGXC76C33F0132070', 'LGXC76C33F0140783', 'LGXC76C34E0047527', 'LGXC76C34F0138153', 'LGXC76C34F0150013', 'LGXC76C35E0099832', 'LGXC76C35E0120730', 'LGXC76C35E0145790', 'LGXC76C35E0214543', 'LGXC76C36E0099838', 'LGXC76C36E0144440', 'LGXC76C36E0162307', 'LGXC76C36E0168804', 'LGXC76C36F0020914', 'LGXC76C36F0050818', 'LGXC76C36F0116610', 'LGXC76C36F0131270', 'LGXC76C36F0148828', 'LGXC76C37F0130418', 'LGXC76C37F0132993', 'LGXC76C38E0126778', 'LGXC76C38E0140616', 'LGXC76C38E0157738', 'LGXC76C38F0009879', 'LGXC76C39F0097678', 'LGXC76C3XE0105656', 'LGXC76C3XE0183564', 'LGXC76C3XF0100264', 'LGXC76C3XF0157757']


def load_center_data(file):
	centers = {}
	with open(file) as fr:
		for line in fr:
			center = Center(line)
			if center.car_id not in centers:
				centers[center.car_id] = center
	return centers


def haversine(center1, center2): # 经度1，纬度1，经度2，纬度2 （十进制度数  
    """ 
    Calculate the great circle distance between two points  
    on the earth (specified in decimal degrees) 

    The units are in meters
    """  
    # 将十进制度数转化为弧度 
    lon1 = center1.longitude
    lat1 = center1.latitude
    lon2 = center2.longitude
    lat2 = center2.latitude
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])  
    # haversine公式  
    dlon = lon2 - lon1   
    dlat = lat2 - lat1   
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2  
    c = 2 * asin(sqrt(a))   
    r = 6371 # 地球平均半径，单位为公里  
    return c * r * 1000  


def caculate(centers, car_id_vector=CAR_ID_VECTOR):
	matrix = np.zeros((len(car_id_vector), len(car_id_vector)))
	for i in range(len(car_id_vector)):
		for j in range(len(car_id_vector)):
			cid_1 = car_id_vector[i]
			cid_2 = car_id_vector[j]
			if cid_1 not in centers or cid_2 not in centers:
				matrix[i][j] = 0
				continue
			u_a = centers[cid_1]
			u_b = centers[cid_2]
			s = exp(- haversine(u_a, u_b)**2 /(2 * alpha**2))
			matrix[i][j] = s
	return matrix


def main():
	living_center = "Data/living_center.txt"
	m = caculate(load_center_data(living_center))
	np.savetxt("living_center_similarity.out", m, fmt='%1.5f', delimiter=",", header=",".join(CAR_ID_VECTOR))


if __name__ == '__main__':
	main()

