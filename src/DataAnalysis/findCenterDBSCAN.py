#coding:utf-8

"""
DBScan 算法寻找汽车用户最常停留区域和充电区域
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN

import conf
from Model.Models import haversine

def tst(v1, v2):
	lon1, lat1 = v1
	lon2, lat2 = v2
	return haversine(lon1, lat1, lon2, lat2)

def find_points_center(stay_points_items, charge_point_items, fw):
	center_point_result = {}
	car_id = stay_points_items["id"]
	points = stay_points_items["points"]
	latitude_vector = np.array([p.GPS.latitude for p in points], dtype=float)
	longitude_vector = np.array([p.GPS.longitude for p in points], dtype=float)
	X = np.c_[longitude_vector, latitude_vector]
	y_pred = DBSCAN(eps=250, metric=tst).fit_predict(X)
	count_tag = sorted([(np.sum(y_pred==i),i) for i in set(y_pred.flat)])
	index_list = list(range(len(count_tag)))
	index_list.reverse()
	for i in index_list:
		item = count_tag[i]
		if item[-1] != -1:
			center_area_tags = [item[-1]]
			if len(count_tag) > 2:
				center_area_tags.append(count_tag[i-1][-1])
			break
	center_position = []
	for center_area_tag in center_area_tags:
		center_point = np.sum(X[y_pred==center_area_tag,:], axis=0)/X[y_pred==center_area_tag,:].shape[0]
		center_position.append(center_point)
	center_point_result["staycenter"] = center_position

	points = charge_point_items["points"]
	if len(points) < 2:
		center_point_result["chargecenter"] = []
	else:
		latitude_vector = np.array([p.GPS.latitude for p in points], dtype=float)
		longitude_vector = np.array([p.GPS.longitude for p in points], dtype=float)
		X = np.c_[longitude_vector, latitude_vector]
		y_pred = DBSCAN(eps=250, metric=tst).fit_predict(X)
		count_tag = sorted([(np.sum(y_pred==i),i) for i in set(y_pred.flat)])
		for i in count_tag[::-1]:
			if i[-1] != -1:
				center_area_tag = i[-1]
				break
		center_point = np.sum(X[y_pred==center_area_tag,:], axis=0)/X[y_pred==center_area_tag,:].shape[0]
		center_point_result["chargecenter"] = center_point
	stay_center_str = ",".join(["%f-%f"%(p[0], p[1]) for p in center_point_result["staycenter"]])
	p = center_point_result["chargecenter"]
	charge_center_str = "%f-%f"%(p[0], p[1]) if len(p)>1 else ""
	fw.write("\t".join([car_id, stay_center_str, charge_center_str]) + "\n")
	return 1













