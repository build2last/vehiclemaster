#coding:utf-8

import unittest
import os
import time

from conf import DRIVING_DATA_PATH, POSTGIS_CON_STR, OUTPUT_STAY_POINT_DIR, POI_PATH, GPDB_CON_STR, OUTPUT_USER_POI_DIR
from conf import POSTGIS_CON_STR
from conf import read_file_paths_from_dir

import conf

from Model.Models import StayPoint, POI
from DataProcess import StayPointExtractor as SP
from DataProcess import UserPOIExtractor as upe
from DataProcess import FeatureMaker as FM
from DataProcess import statisticForCheck as sta
from DataProcess import TraceExtractor

from DataAnalysis import LDA_POI_model as lda
from DataAnalysis import findCenterDBSCAN

"""
Test module for develop & set up test.

目前重复建表会报错--2018-01-11
在测试中注释掉了所有涉及数据库得到操作
"""


class TestDataProcess(unittest.TestCase):
	@unittest.skip("demonstrating skipping")
	def test_stayPointExtractor(self):
		"""
			对单个文件，测试停留点提取模块的工作
		"""
		drive_data_file_paths = read_file_paths_from_dir(DRIVING_DATA_PATH)
		for path in drive_data_file_paths:
			self.assertTrue(SP.stay_point_detect(path, to_dir=OUTPUT_STAY_POINT_DIR))

	@unittest.skip("demonstrating skipping")
	def test_charging_point_extract(self):
		drive_data_file_paths = read_file_paths_from_dir(DRIVING_DATA_PATH)
		for path in drive_data_file_paths:
			self.assertTrue(SP.charging_point_detect(path, to_dir=conf.CHARGING_POINTS_PATH))


	@unittest.skip("demonstrating skipping")
	def test_database_init_and_load(self):
		"""
			数据库操作测试：
			测试建立停留点表格，并将停留点数据导入
		"""
		#self.assertTrue(SP.create_staypoints_table(POSTGIS_CON_STR))
		self.assertTrue(SP.dir2db(conf.CHARGING_POINTS_PATH, GPDB_CON_STR, "vehicle_chargepoint"))
	
	@unittest.skip("demonstrating skipping")
	def test_UserPOIExtractor(self):
		"""
			提取用户兴趣点，即在停留点基础上加入地理位置标签信息。
			数据库操作：
				创建POI表格并导入源数据，即网上的标签信息；
		"""
		for p in read_file_paths_from_dir(OUTPUT_STAY_POINT_DIR, extension=".StayPoint"):
			self.assertTrue(
				upe.knn_extract(POI_PATH, StayPoint.load(p), conf.OUTPUT_USER_POI_DIR)
			)
		#self.assertTrue(upe.create_POI_table(POSTGIS_CON_STR))

		# p = POI_PATH
		# self.assertTrue(upe.dump_data_to_database(p, POSTGIS_CON_STR))

	@unittest.skip("demonstrating skipping")
	def test_Feature_Maker(self):
		"""
			将每个用户的兴趣点转为向量形式，制作POI（M*N）矩阵，并将特征矩阵持久化存储，便于后续计算
			M：用户数量
			N：标签种类
		"""
		self.assertTrue(FM.dump_feature_vector_list())

	@unittest.skip("demonstrating skipping")
	def test_lda_analyse(self):
		"""
			对特征矩阵用LDA的方法挖掘出主题模型。
		"""
		self.assertTrue(lda.run_lda(conf.TAGS))

	@unittest.skip("demonstrating skipping")
	def test_center_find(self):
		stay_point_files = read_file_paths_from_dir(OUTPUT_STAY_POINT_DIR, extension=".StayPoint")
		charging_point_files = read_file_paths_from_dir(conf.CHARGING_POINTS_PATH, extension=".ChargePoint")
		fw = open(conf.CENTER_POSITION, "w")
		for i in range(len(stay_point_files)):
			stay_points = StayPoint.load(stay_point_files[i])
			charge_points = StayPoint.load(charging_point_files[i])
			findCenterDBSCAN.find_points_center(stay_points, charge_points, fw)
		fw.close()	

def simple_test():
	# data_file_paths = read_file_paths_from_dir(conf.DRIVING_DATA_PATH, "txt")
	# for file_path in data_file_paths:
	# 	TraceExtractor.trace_point_extract(file_path, to_dir=conf.TRACE_DIR)
	data_file_paths = read_file_paths_from_dir(conf.TRACE_DIR, "trace")
	for file_path in data_file_paths:
		TraceExtractor.trace_file_analyse(file_path, conf.TRACE_STA_DIR)



if __name__ == '__main__':
	tick = time.time()
	#unittest.main()
	simple_test()
	tock = time.time()
	print("--------------\nCost time: %ds"%(int(tock-tick)))