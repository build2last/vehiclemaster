#coding:utf-8
# Author:	   		   liu kun
# Start writing in 2018-01, Shanghai
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

from datetime import datetime
import time
import os
from conf import *
from conf import TAGS, TAG_TABLE
"""数据抽象层


设计动机：
 将文本记录抽象成具有统一结构的程序对象，方便后续的分析工作的展开；
 更灵活快速地应对外部数据格式发生的变化。


数据抽象层基本对象

Tag：从参考数据源（百度地图）获取的地理标签数据；
GPS
StayPoint
POI:		Point of interest
"""


class Tag:
	def __init__(self, tag_str):
		if tag_str.isdigit():
			tag_num = int(tag_str)
			self.tag = TAGS[tag_num-1]
			self.id = tag_num
		else:
			try:
				self.id = TAG_TABLE[tag_str]
				self.tag = tag_str
			except Exception as e:
				print(e, "happened for ",tag_str)

	def __str__(self):
		return self.tag


class GPS:
	"""GPS object
	"""
	def __init__(self, time, longitude, latitude):
		# Load from raw text type data.
		self.time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
		self.longitude = float(longitude)
		self.latitude = float(latitude)
	def to_csv_line_str(self, line):
		return ",".join([self.time.strftime('%Y-%m-%d %H:%M:%S'), self.longitude+" "+self.latitude])


class StayPoint:
	"""Stay Point object: where car stop?

	@GPS
	@stay time: stay time in minute
	@state:		1 run, 0 stop, 2 charging
	"""
	@staticmethod
	def load(sp_file_path):
		s_points = []
		car_id = os.path.split(sp_file_path)[-1].split(".")[0]
		with open(sp_file_path) as fr:
			for line in fr:
				s_points.append(StayPoint(line))
		return {"points":s_points, "id":car_id}


	def __init__(self,line):
		if isinstance(line, str):
			values = line.split("\t")
			self.GPS = GPS(*values[0:3])
			self.staytime = int(values[-2])
			self.state = int(values[-1])

	def as_dic(self):
		return {
			"time":			self.GPS.time.strftime( '%Y-%m-%d %H:%M:%S' ),
			"longitude":	self.GPS.longitude,
			"latitude":		self.GPS.latitude,
			"staytime":		self.staytime,
			"status":		self.state
		}

	def to_str(self):
		return self.GPS.time.strftime( '%Y-%m-%d %H:%M:%S' )+"\t"+str(self.GPS.longitude)+"\t"+str(self.GPS.latitude)+"\t"+str(self.staytime)+"\t"+str(self.state)


class POI:
	"""Point of interest

	对poi数据进行抽象，并向上提供数据接口。
	"""
	@staticmethod
	def load(data_file_path):
		points = []
		car_id = os.path.split(data_file_path)[-1].split(".")[0]
		with open(data_file_path) as fr:
			for line in fr:
				points.append(POI(line))
		return {"points":points, "id":car_id}

	def __init__(self, line):
		values = line.strip().split("\t")
		self.GPS = GPS(*values[:3])
		self.staytime = int(values[3])
		self.status = int(values[4])
		if ";" in line:
			self.tags = [Tag(tag) for tag in values[5].split(";")]
		else:
			self.tags = []


class TraceLogDataItem:
	"""
	抽象原始轨迹数据

	Abstract layer for raw data
	"""

	@staticmethod
	def args_generator(args):
		count = 0
		length = len(args)
		while count < length:
			yield args[count]
			count += 1

	@staticmethod
	def load(log_file):
		log_item = []
		car_id = os.path.split(log_file)[-1].split(".")[0]
		with open(log_file) as fr:
			for line in fr:
				log_item.append(TraceLogDataItem(line))
		return {"points":log_item, "id":car_id}


	def __init__(self, args_ge):
		if isinstance(args_ge, str):
			values = args_ge.strip().split(",")
			args_ge = self.args_generator(values)
		else:
			return None
		self.collect_time = datetime(*time.strptime(next(args_ge), "%Y-%m-%d %H:%M:%S")[:6])
		self.total_mileage = float(next(args_ge))
		self.longitude_flag = next(args_ge)
		self.alm_motor_controller_temperature = next(args_ge)
		self.alm_drive_motor_temperature = next(args_ge)
		self.alm_motor_driving_system_fault = next(args_ge)
		self.alm_dcdc_temperature = next(args_ge)
		self.alm_dcdc_state = next(args_ge)
		self.alm_total_voltage_battery_pack = next(args_ge)
		self.alm_single_pack_maximum_temperature = next(args_ge)
		self.alm_single_pack_minimum_temperature = next(args_ge)
		self.alm_single_pack_maximum_voltage = next(args_ge)
		self.alm_single_pack_minimum_voltage = next(args_ge)
		self.alm_high_pressure_interlock_state = next(args_ge)
		self.alm_value_of_insulation_resistance = next(args_ge)
		self.alm_collision_signal_state = next(args_ge)
		self.alm_energy_storage_system_fault = next(args_ge)
		self.alm_ABS_system_fault = next(args_ge)
		self.positioning_state = next(args_ge)
		self.latitude_flag = next(args_ge)
		self.longitude = float(next(args_ge))/1000000
		self.latitude = float(next(args_ge))/1000000
		self.speed = float(next(args_ge))
		self.direction = next(args_ge)
		self.motor_controlling_temperature = next(args_ge)
		self.drive_motor_speed = next(args_ge)
		self.drive_motor_temperature = next(args_ge)
		self.motor_bus_current = next(args_ge)
		self.accelerator_pedal_stroke = next(args_ge)
		self.brake_pedal_state = next(args_ge)
		self.power_system_ready = next(args_ge)
		self.urgent_cut_off_power_request = next(args_ge)
		self.high_voltage_current_of_battery = next(args_ge)
		self.soc = next(args_ge)
		self.residual_energy = next(args_ge)
		self.total_battery_voltage = next(args_ge)
		self.single_pack_maximum_temperature = next(args_ge)
		self.single_pack_minimum_temperature = next(args_ge)
		self.single_pack_maximum_voltage = next(args_ge)
		self.single_pack_minimum_voltage = next(args_ge)
		self.value_of_insulation_resistance = next(args_ge)
		self.battery_equalization_activation = next(args_ge)
		self.max_temperature_battery_pack = next(args_ge)
		self.min_temperature_battery_pack = next(args_ge)
		self.start_time = next(args_ge)
		if self.start_time:
			self.start_time = datetime(*time.strptime(self.start_time, "%Y-%m-%d %H:%M:%S")[:6])
		self.liquid_fuel_consumption = next(args_ge)
		self.status = next(args_ge)
		self.end_time = next(args_ge)
		if self.end_time:
			self.end_time = datetime(*time.strptime(self.end_time, "%Y-%m-%d %H:%M:%S")[:6])
		self.vehile_status = next(args_ge) # 0：停止；1：行驶；2：充电
		self.staytime = -1

	def get_raw_line(self):
		return self.raw_line.strip()

	def as_stay_point_str(self):
		return "%s\t%f\t%f\t%d\t%d"%(
			self.collect_time.strftime("%Y-%m-%d %H:%M:%S"),
			self.longitude, self.latitude, self.staytime, int(self.vehile_status))

"""
# -------------------
# 下面是一些地理计算方法
# 主要包括：
#  GPS椭球面距离计算
"""
from math import radians, cos, sin, asin, sqrt  
  
def haversine(lon1, lat1, lon2, lat2): # 经度1，纬度1，经度2，纬度2 （十进制度数  
    """ 
    Calculate the great circle distance between two points  
    on the earth (specified in decimal degrees) 

    The units are in meters
    """  
    # 将十进制度数转化为弧度  
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])  
  
    # haversine公式  
    dlon = lon2 - lon1   
    dlat = lat2 - lat1   
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2  
    c = 2 * asin(sqrt(a))   
    r = 6371 # 地球平均半径，单位为公里  
    return c * r * 1000  