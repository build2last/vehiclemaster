#coding:utf-8
#Python 2/3 compatible
from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

__author__ = "Liu Kun"
__start_date__ = "2017-12-18"
__python__ = [2.7, 3]

import os

OUTPUT = "OUTPUT"

CENTER_POSITION = os.path.join(OUTPUT, "center.out")

POSTGIS_CON_STR = "dbname='road_network' user='postgres' host='localhost' password=12342234"

GPDB_CON_STR = "dbname='gpdb' user='user1' host='localhost' port=10864 password=user1"

DRIVING_DATA_PATH = "RawData/比亚迪50辆车数据副本"

MIDDLE_DATA_DIR = "MiddleData"

OUTPUT_STAY_POINT_DIR = os.path.join(MIDDLE_DATA_DIR, "staypoint")

TRACE_DIR = os.path.join(MIDDLE_DATA_DIR, "trace")

TRACE_STA_DIR = os.path.join(MIDDLE_DATA_DIR, "trace", "statistic")

POI_CATAGORY_PATH = os.path.join("RawData", "LocationTag", "POICategory.txt")

POI_PATH = os.path.join("RawData", "LocationTag", "locationTags.txt")

OUTPUT_USER_POI_DIR = os.path.join(MIDDLE_DATA_DIR, "poi")

OUT_PUT_ANALYSE = os.path.join(OUTPUT, "analyse")

POI_FEATURE_MATRIX = os.path.join(MIDDLE_DATA_DIR, "Feature", "POI_words_matrix.pkl")

COORDINATE_SEGMENT_DISTANCE = 0.001

BOUND_DISTANCE = 50

CHARGING_POINTS_PATH = os.path.join(MIDDLE_DATA_DIR, "charging")

# Values in raw GPS log text data
# Collect time
COLLECTTIME = 0
# Mileage: unit is km
MILEAGE = 1
LONGITUDE = 20
LATITUDE = 21
# km/h
SPEED = 22
#Battery
SOC = 33
START_TIME = 44
END_TIME = 47
# 0：停止；1：行驶；2：充电；
STATUS = 48


#从参考数据源（百度地图）获取的地理标签数据；
TAGS = [
	"停车场","医院","学校","政府机构","加油站","旅游景点",
	"教育培训","汽车服务","购物中心","商铺","酒店","美食","公司",
	"休闲娱乐","交通设施","住宅 超市 生活服务","运动健身","文化传媒",
	"购物","生活服务","房地产","购物","医疗","金融","出入口","丽人",
	"行政地标","门址"
]
TAG_TABLE = {j:i for i,j in enumerate(TAGS)}


def read_file_paths_from_dir(dir_path, extension=".txt"):
	# Remove system file in Unix directory
	data_files = filter(lambda p:p.endswith(extension), os.listdir(dir_path))
	return [os.path.join(dir_path, x) for x in data_files]